function SearchUnclosedBack(pattern, pattern2)

  " Store current pos
  let l:matchpos = getpos('.')

  " Initialize return
  let l:ret = 0 

  " Search Pattern, no wrap
  while search(a:pattern2, "W", "", "") > 0 

    let l:foundpos = getpos('.')
    " Search for pair only in current line
    if eval(searchpair(a:pattern, '', a:pattern2, 'b', '', line(".")))
      call setpos('.', l:foundpos)
      continue
    endif

    " found store position and line
    let l:matchpos = getpos('.')
    let l:ret = line(".")
    break
  endwhile

  " Set cursor to position and return
  call setpos('.', l:matchpos)
  return l:ret
endfunction
