:syntax on        "Turn on syntax highlighting
:set laststatus=2 "Always show status line

:set autowrite    "Automatically write a file when leaving a modified buffer
:set confirm      "Start a dialog when a command fails (here when quit command fails)
:set tabstop=2    "Number of spaces a TAB in the text stands for

:set shiftwidth=2 "Number of spaces used for each step of (auto)indent
:set hlsearch     "Have vim highlight the target of a search
:set incsearch    "Do incremental searching

:set ruler        "Show the cursor position all the time
:set number       "Show line numbers
:set relativenumber       "Show line numbers
:set ignorecase   "Ignore case when searching

:set title        "Show info in the window title
:set titlestring=PANKAJ:\ %F
   "Automatically set screen title


"Indent only if the file is of type cpp,c,java,sh,pl,php,asp
":au FileType cpp,c,java,sh,pl,php,asp  set autoindent
":au FileType cpp,c,java,sh,pl,php,asp  set smartindent
:au FileType cpp,c,java,sh,pl,php,asp  set cindent
:set cinoptions={0s,t0,f0s,g0,i0,(0,=1s,n1s
:set indentexpr=Indent(line(\".\"))

"Wrapping long lines
":set wrapmargin=4   "Margin from the right in which to break a line. Set this value to 4 or 5

":set textwidth=70   "Line length above which to break a line

"Defining abbreviations
:ab #d #define
:ab #i #include


"Defining abbreviations to draw comments
:ab #b /********************************************************
:ab #e ********************************************************/
:ab #l /*------------------------------------------------------*/

"Converting tabs to spaces
:set expandtab

if has("autocmd")
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

"colorscheme
set background=dark
let g:solarized_termtrans = 1
let g:solarized_termcolors=256
colorscheme solarized

set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8


au BufNewFile,BufRead *.txx set filetype=cpp
map <F12> gg=G\|:3,12>\|:10,12> <Enter> 
map <F11> :so ~/.vim/style/kitwareStyleRepl <Enter>
map <F10> :%s/\s\+$//e <Enter> :%s/\(\n\n\)\n\+/\1/e<Enter>
map <F9>  /\%82v.*/ <Enter>
map <F8> gg=G 
map <F7> :echo synIDattr(synID(line("."), col("."), 0), "name") <Enter>
map <F6> /[^\x00-\x7F]<Enter>
map <F4> :set nonumber <Enter>
map <F3> :set norelativenumber <Enter>


set t_RS=
set t_SH=
