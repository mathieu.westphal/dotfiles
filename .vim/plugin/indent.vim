function Indent(line)
    " Store current pos
    let l:indent = cindent(a:line)
    let l:unclosed = Unclosed(a:line)
    let l:lineprec = a:line - 1
    if l:unclosed
      let l:length = strlen(getline(a:line))
      let l:curindent = indent(a:line)
      let l:nextlength = l:length - l:curindent + l:indent
      if l:nextlength > 80
        if l:indent > indent(l:lineprec)
          let l:indent = indent(l:lineprec) + &shiftwidth
        endif
      endif
    endif
    let l:linefirst = split(getline(a:line), " ")[0]
    if l:linefirst ==# "{"
      let l:case = split(getline(l:lineprec), " ")[0]
      if l:case ==# "case"
        let l:indent = indent(l:lineprec) + &shiftwidth
      endif
    endif
    return l:indent
endfunction
