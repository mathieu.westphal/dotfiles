function Unclosed(line)

    let l:linelist = [ 0, a:line, 0, 0]
    call setpos('.', l:linelist)
    let l:foundpos = getpos('.')
    
    " Initialize return
    let l:ret = 0

    if SearchUnclosedBack('(',')')
      let l:pair = eval(searchpair('(', '', ')', 'bW', ''))
      if l:pair <= a:line
        let l:ret = a:line
      endif
    endif
    
    " Set cursor to position and return
    call setpos('.', l:linelist)
    return l:ret
endfunction
