function Repl(pattern, patternSub, col)

    " Move to top of the file
    normal 1G

    " Search Pattern, no wrap
    while search(a:pattern, "W", "", "") > 0

        let l:col = col(".")
        call cursor(0, l:col + a:col)

        " If found pattern is a comment, skip
        if eval(synIDattr(synID(line("."), col("."), 0), "name") =~? "Comment")
            continue
        endif

        " If found pattern is a string, skip
        if eval(synIDattr(synID(line("."), col("."), 0), "name") =~? "String")
            continue
        endif
 
        " If found pattern is a char, skip
        if eval(synIDattr(synID(line("."), col("."), 0), "name") =~? "cCharacter")
            continue
        endif
        
        call cursor(0, l:col)
        let l:col = col(".")

        " Replace pattern by it's substitute
        exec '.s/'.a:pattern.'/'.a:patternSub.'/'

        " Restore cursor position
        normal ``
    endwhile
endfunction

command! -nargs=+ -complete=command Repl call Repl(<args>)
