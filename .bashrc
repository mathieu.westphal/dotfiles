#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \w]\$ '

export EDITOR=vim
export CXXFLAGS="-fdiagnostics-color=always"
export QT_AUTO_SCREEN_SCALE_FACTOR=0

alias imgclip='xclip -selection clipboard -t image/png -i'
alias clf='(shopt -s globstar; clang-format -i **/*.{h,cxx})'
alias fg='function _fg(){ find . -name "*$1*" -print ; }; _fg'
